# README #

### What is this repository for? ###

* Studies for datastructure using javascript & java
* Things I got known about Javascript
* Things I got known about Java 
* Codes from Leetcode ("https://leetcode.com/")

### How do I get set up? ###

* Nothing ! Just look around and share (I hope)



# Javascript I Got Known  #

### Javascript string to array conversion ###
http://stackoverflow.com/questions/13272406/javascript-string-to-array-conversion
### Javascript split string into segments n characters long ###
http://stackoverflow.com/questions/6259515/javascript-elegant-way-to-split-string-into-segments-n-characters-long

### Concept : polyfill, shim ###
https://firejune.com/1732/HTML5+%EC%A0%9C%EB%8C%80%EB%A1%9C+%EC%95%8C%EA%B3%A0+%EC%94%81%EC%8B%9C%EB%8B%A4!+-+html5please.us